# Advego Plagiatus, упакованный в deb-пакет для работы под Wine на Linux

![screenshot](img/img_10.png) 

Бранч `master` — рабочая упаковка, именно так собран готовый deb-пакет, доступный либо в [тегах (релизах)](https://gitlab.com/nixtux-packaging/wined-advego-plagiatus/tags), либо из репозитория [ppa:mikhailnov/utils](https://launchpad.net/~mikhailnov/+archive/ubuntu/utils/). 

Deb-пакет содержит в себе:

* [скрипт запуска](https://gitlab.com/nixtux-packaging/wined-advego-plagiatus/blob/master/usr/bin/wined-advego-plagiatus), который настраивает префикс wine и запускает программу в нем
* desktop-файлы для интеграции программы в нативные меню приложений

Бранч `wined-dev` — попытка централизировать запуск различных приложений в Wine через единую систему `wined`, завернув программы в AppArmor или FireJail для макисмального ограничения их функционала. Пока что совсем не доделано и не работает.

Если кто хочет присоединиться или, например, прислать спеки RPM, pkgbuild-ы — делайте мерж-реквесты или пишите в [Issues](https://gitlab.com/nixtux-packaging/wined-advego-plagiatus/issues) 

## Как собрать этот deb-пакет самому?
```
sudo apt install devscripts git
git clone https://gitlab.com/nixtux-packaging/wined-advego-plagiatus.git
cd wined-advego-plagiatus
dpkg-buildpackage
```

## Косяки
* пакет архитектурно зависем, хотя по факту нужно только i386
* зависимости от wine не проверял на 32-битной архитектуре
* куски префикса wine типа `C:\Windows` хранятся все ранво в домашней папке пользователя. Их бы хранить в пакете, в к хомяку присоединять симлинками. Но как тогда быть при смене версий wine, ведь набор этих библиотек свой для каждой версии.